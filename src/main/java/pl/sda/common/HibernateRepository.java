package pl.sda.common;

import java.util.List;

public interface HibernateRepository<T> {

    List<T> findAll();

    T findById(Integer id);

    T save(T entity);

    void remove(T entity);
}
