package pl.sda.common;

import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.util.HibernateUtils;

import java.util.List;

@AllArgsConstructor
public class AbstractHibernateRepository<T> implements HibernateRepository<T> {

    private Class<T> clazz;

    @Override
    public List<T> findAll() {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        Query<T> query = session.createQuery("select e from " + clazz.getSimpleName() + " e", clazz);

        List<T> resultList = query.getResultList();

        transaction.commit();
        session.close();

        return resultList;
    }

    @Override
    public T findById(Integer id) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        T result = session.get(clazz, id);

        transaction.commit();
        session.close();

        return result;
    }

    @Override
    public T save(T entity) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        T result = (T) session.merge(entity);

        transaction.commit();
        session.close();

        return result;
    }

    @Override
    public void remove(T entity) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        session.remove(entity);

        transaction.commit();
        session.close();
    }
}
