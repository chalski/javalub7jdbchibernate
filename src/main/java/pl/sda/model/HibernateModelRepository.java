package pl.sda.model;

import pl.sda.common.AbstractHibernateRepository;

import java.util.List;

public class HibernateModelRepository extends AbstractHibernateRepository<Model> {
    public HibernateModelRepository() {
        super(Model.class);
    }


    public static void main(String[] args) {
        HibernateModelRepository repository = new HibernateModelRepository();

        List<Model> all = repository.findAll();

        System.out.println(all);
    }
}
