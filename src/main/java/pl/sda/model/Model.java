package pl.sda.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.brand.Brand;
import pl.sda.engine.Engine;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    // Cascade przypadek 2 cascade = REMOVE
//    @ManyToOne(cascade = CascadeType.REMOVE)
    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    private LocalDate dateOfCreation;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "model_engine",
            joinColumns = {@JoinColumn(name = "model_id")},
            inverseJoinColumns = {@JoinColumn(name = "engine_id")}
    )
    private Set<Engine> engines;

    @Override
    public String toString() {
        return "Model[name = " +name +"]";
    }

}
