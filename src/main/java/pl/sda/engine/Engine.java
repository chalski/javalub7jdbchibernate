package pl.sda.engine;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Data
@Entity
public class Engine {

    @Id
    private Integer id;
    private String name;
    private Double capacity;
    private Integer power;
    @Enumerated(EnumType.STRING)
    private FuelType type;
}
