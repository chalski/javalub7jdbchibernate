package pl.sda.engine;

import pl.sda.common.AbstractHibernateRepository;

public class HibernateEngineRepository extends AbstractHibernateRepository<Engine> {
    public HibernateEngineRepository() {
        super(Engine.class);
    }

    public static void main(String[] args) {
        System.out.println(new HibernateEngineRepository().findAll());
    }
}
