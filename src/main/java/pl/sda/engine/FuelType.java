package pl.sda.engine;

public enum FuelType {
    GASOLINE,
    DIESEL,
    GAS,
    HYBRID,
    OTHER
}
