package pl.sda.repotest;

import pl.sda.brand.Brand;
import pl.sda.brand.BrandRepository;
import pl.sda.brand.JDBCBrandRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCRepositoryTest {

    public static void main(String[] args) {

        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javalub7", "chalski_homeb", "homebudget");) {
            BrandRepository brandRepository = new JDBCBrandRepository(connection.createStatement());

            Brand brand = new Brand();
//            brand.setId(17);
            brand.setName("Opel");
            brandRepository.save(brand);

            System.out.println(brandRepository.findAll());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}