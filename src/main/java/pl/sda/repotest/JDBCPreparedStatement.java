package pl.sda.repotest;

import java.sql.*;

public class JDBCPreparedStatement {

    public static void main(String[] args) {
        try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javalub7?allowMultiQueries=true", "sda", "pass");) {
            doSelect(connection, "Audi");
            System.out.println("========================");
            doSelect(connection, "' OR '1' = '1");
            doSelect(connection, "'; DELETE FROM user WHERE '1'='1");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void doSelect(Connection connection, String brandName) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("select * from brand where name = ?");
        statement.setString(1, brandName);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            System.out.println(resultSet.getInt(1));
            System.out.println(resultSet.getString(2));
        }
    }

}
