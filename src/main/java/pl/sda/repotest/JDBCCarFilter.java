package pl.sda.repotest;

import pl.sda.car.CarFilter;
import pl.sda.car.CarRepository;
import pl.sda.engine.FuelType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCCarFilter {

    public static void main(String[] args) {

        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javalub7", "sda", "pass");) {
            CarRepository carRepository = new CarRepository(connection);

            CarFilter filter = new CarFilter();
            filter.setBrandName("Audi");
            filter.setModelName("A4");
            filter.setCapacityFrom(1.6);
            filter.setCapacityTo(2.0);
            filter.setPowerFrom(100);
            filter.setPowerTo(200);
            filter.getFuelTypes().add(FuelType.DIESEL);
            filter.getFuelTypes().add(FuelType.GASOLINE);
            System.out.println(carRepository.findByFilter(filter));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}