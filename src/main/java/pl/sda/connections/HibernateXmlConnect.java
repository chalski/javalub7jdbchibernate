package pl.sda.connections;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateXmlConnect {

    public static void main(String[] args) {
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .configure("my-hibernate-config.xml").build();

        Metadata metadata = new MetadataSources(serviceRegistry)
                .getMetadataBuilder().build();

        SessionFactory sessionFactory = metadata.buildSessionFactory();

        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.getTransaction();
        transaction.begin();



        transaction.commit();
        currentSession.close();

//        List<Brand> resultList = HibernateUtils.doInTransaction(currentSession,
//                session -> session.createQuery("select b from Brand b", Brand.class).getResultList());
//        System.out.println(resultList);

        sessionFactory.close();
    }

}
