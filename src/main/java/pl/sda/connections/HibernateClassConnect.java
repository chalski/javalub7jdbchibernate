package pl.sda.connections;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import pl.sda.brand.Brand;
import pl.sda.car.Car;
import pl.sda.engine.Engine;
import pl.sda.model.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HibernateClassConnect {

    public static void main(String[] args) {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

        Map<String, String> properties = new HashMap<>();
        properties.put(Environment.URL, "jdbc:mysql://localhost:3306/javalub7");
        properties.put(Environment.USER, "sda");
        properties.put(Environment.PASS, "pass");
        properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        properties.put(Environment.POOL_SIZE, "1");
        properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        properties.put(Environment.SHOW_SQL, "true");
        properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");

        registryBuilder.applySettings(properties);

        StandardServiceRegistry serviceRegistry = registryBuilder.build();

        Metadata metadata = new MetadataSources(serviceRegistry)
            .addAnnotatedClass(Brand.class)
                .addAnnotatedClass(Car.class)
                .addAnnotatedClass(Model.class)
                .addAnnotatedClass(Engine.class)
                .buildMetadata();

        SessionFactory sessionFactory = metadata.buildSessionFactory();

        Session session = sessionFactory.getCurrentSession();

        Transaction transaction = session.beginTransaction();

        // :brandName pozwala na zabezpieczenie się przed SQL-Injection
        // Nie powinniśmy sklejać stringów

        List<Model> engines = session
                .createQuery("select m from Model m where m.brand.name = :order " +
                        "order by m.name", Model.class)
                .setParameter("order", "Audi")
                .getResultList();

        transaction.commit();

        System.out.println(engines);

        sessionFactory.close();
    }

}
