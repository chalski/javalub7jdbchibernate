package pl.sda.connections;

import java.sql.*;

public class JDBCConnect{

    public static void main(String[] args) {
        try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javalub7", "chalski_homeb", "homebudget");) {
            Statement statement = connection.createStatement();

            System.out.println("!!!INSTERT!!!");
            statement.execute("INSERT INTO BRAND(name) VALUES('BMW')");
            showBrands(statement);

            System.out.println("!!!UPDATE!!!");
            statement.executeUpdate("UPDATE BRAND set name = 'Porshe' WHERE name = 'BMW'");
            showBrands(statement);

            System.out.println("!!!REMOVE!!!");
            statement.executeUpdate("DELETE FROM BRAND WHERE name = 'Porshe'");
            showBrands(statement);

            statement.execute("CREATE TABLE TEST(id INT(6))");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void showBrands(Statement statement) throws SQLException {
        ResultSet resultSet = statement.executeQuery("select * from brand");
        while (resultSet.next()){
            System.out.println(resultSet.getInt(1));
            System.out.println(resultSet.getString(2));
        }
    }


}
