package pl.sda.brand;

import pl.sda.common.HibernateRepository;

import java.util.List;

public interface BrandRepository extends HibernateRepository<Brand> {

    List<Brand> findByName(String name);

}