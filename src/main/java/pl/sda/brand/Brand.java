package pl.sda.brand;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import pl.sda.model.Model;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(exclude = "models")
@NoArgsConstructor
@Entity
//@Table(name = "brands")
public class Brand {

    @Id
    @GenericGenerator(name = "gen", strategy = "increment")
    @GeneratedValue(generator = "gen", strategy = GenerationType.IDENTITY)
    private Integer id;
//    @Column(name = "brand_name")
    private String name;

    // Cascade przypadek 1, Cascade = MERGE
    // Cascade przypadek 3, Cascade = REMOVE
    @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Model> models;

    @Override
    public String toString() {
        return "Brand[name = " +name +"]";
    }
}
