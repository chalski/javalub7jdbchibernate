package pl.sda.brand;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.common.AbstractHibernateRepository;
import pl.sda.util.HibernateUtils;

import java.util.List;

public class HibernateBrandRepository extends AbstractHibernateRepository<Brand> implements BrandRepository {

    public HibernateBrandRepository() {
        super(Brand.class);
    }

    @Override
    public List<Brand> findByName(String nameArg) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        Query<Brand> query = session.createQuery("select b from Brand b where b.name = :nameParam", Brand.class);
        query.setParameter("nameParam", nameArg);
        List<Brand> resultList = query.getResultList();

        transaction.commit();
        session.close();

        return resultList;
    }

    public static void main(String[] args) {
        HibernateBrandRepository repository = new HibernateBrandRepository();

        System.out.println(repository.findByName("Audi"));

        HibernateUtils.close();
    }
}
