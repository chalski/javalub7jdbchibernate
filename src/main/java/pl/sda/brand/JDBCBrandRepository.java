package pl.sda.brand;

import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class JDBCBrandRepository implements BrandRepository {

    private Statement statement;

    @Override
    public List<Brand> findAll() {
        try {
            ResultSet resultSet = statement.executeQuery("select * from brand");
            List<Brand> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(mapEntity(resultSet));
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private Brand mapEntity(ResultSet resultSet) throws SQLException {
        Brand brand = new Brand();
        brand.setId(resultSet.getInt(1));
        brand.setName(resultSet.getString(2));
        return brand;
    }

    @Override
    public Brand findById(Integer id) {
        try {
            ResultSet resultSet = statement.executeQuery("select * from brand where id = " + id);
            if (resultSet.next()) {
                return mapEntity(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Brand save(Brand brand) {
        try {
            if (brand.getId() == null) {
                statement.executeUpdate("INSERT INTO brand(name)" +
                        "VALUES('" + brand.getName() + "' );");
            } else {
                statement.executeUpdate("UPDATE BRAND SET name = '"+brand.getName()+"' WHERE id = " + brand.getId() );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void remove(Brand brand) {

    }

    @Override
    public List<Brand> findByName(String name) {
        return null;
    }
}
