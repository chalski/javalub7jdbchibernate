package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.brand.Brand;
import pl.sda.util.HibernateUtils;

public class LazyLoading {

    public static void main(String[] args) {

        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        Brand brand = session.get(Brand.class, 13);
        // Wyciągnneliśmy Markę

        System.out.println(brand.getModels());
        //Lazy initalization exception, wyciągnięte poza sesją

        transaction.commit();
        session.close();


        Session session2 = HibernateUtils.getSession();
        Transaction transaction2 = session2.beginTransaction();

        Brand brand2 = session2.get(Brand.class, 13);
        // Wyciągneliśmy Markę
        System.out.println(brand2.getModels());
        // Wyciągneliśmy modele

        transaction2.commit();
        session2.close();

        System.out.println(brand2.getModels());
        //Tutaj to działa

        Session session3 = HibernateUtils.getSession();
        Transaction transaction3 = session3.beginTransaction();

        Brand brand3 = session3.createQuery("select b from Brand b join fetch b.models where b.id = 13", Brand.class).getSingleResult();
        // Zrobi joina co zmniejsza liczbę zapytań do bazy danych.

        transaction3.commit();
        session3.close();

        System.out.println(brand3.getModels());

        HibernateUtils.close();
    }

}
