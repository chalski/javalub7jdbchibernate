package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.brand.Brand;
import pl.sda.util.HibernateUtils;

public class DirtyCheckingTest {

    public static void main(String[] args) {

        Session session = HibernateUtils.getSession();

        Transaction transaction = session.getTransaction();
        transaction.begin();
//      Równoważne z
//        session.beginTransaction();

        Brand fiat = new Brand();
        fiat.setName("Fiat");
        // Encja nie zapisała się
        // Jest w stanie transient

        Brand renault = new Brand();
        renault.setName("Renault");
        session.persist(renault);
        // Encja zapisała się
        // Jest w stanie persistent

        Brand toyota = session.get(Brand.class, 15);
        toyota.setName("Lexus");
        // Encja zmieniła swój stan i zrobiła update
        // Jest w stanie persistent
        // Zadziała mechanizm dirty checking

        transaction.commit();
        session.close();

        toyota.setName("Kia");
        // Encja nie zmieniła się
        // Jest w stanie detached

        Session otherSession = HibernateUtils.getSession();
        Transaction otherTransaction = otherSession.beginTransaction();

        // otherSession.persist(toyota);
        // to rzuci wyjątek org.hibernate.PersistentObjectException: detached entity passed to persist: pl.sda.brand.Brand

        toyota.setName("Honda");
        // Encja nie zmieniła się
        // Encja jest dalej w stanie detached

        Brand honda = (Brand) otherSession.merge(toyota);
        // Teraz encja jest Hondą
        // toyota jest dalej deatched
        // ale honda jest persistent

        toyota.setName("Trabant");
        // Encja się nie zmieniła
        // Ciągle jest detached

        honda.setName("Aston Martin");
        // Encja się zmieniła
        // Bo jest w stanie persistent

        otherTransaction.commit();
        otherSession.close();

        HibernateUtils.close();

    }

}
