package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.brand.Brand;
import pl.sda.model.Model;
import pl.sda.util.HibernateUtils;

import java.util.HashSet;
import java.util.Set;

public class Cascade {

    public static void main(String[] args) {

        // Przypadek 1
        // Brand ma cascade MERGE na models
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        Brand brand = new Brand();
        brand.setName("Volkswagen");

        Model model1 = new Model();
        model1.setName("Golf");
        model1.setBrand(brand);

        Model model2 = new Model();
        model2.setName("Passat");
        model2.setBrand(brand);

        Set<Model> models = new HashSet<>();
        models.add(model1);
        models.add(model2);
        brand.setModels(models);

        session.merge(brand);

        // Model ma cascade=REMOVE na brand
        // Przypadek 2
        transaction.commit();
        session.close();

        Session session1 = HibernateUtils.getSession();
        Transaction transaction1 = session1.beginTransaction();

//        Model mergedModel = (Model) session1.merge(model2);
//        session1.remove(mergedModel); // Model usunie encję Brand jeśli baza na to pozwoli

        transaction1.commit();
        session1.close();

        // Brand ma cascade=REMOVE na models
        // Przypadek 3
        Session session2 = HibernateUtils.getSession();
        Transaction transaction2 = session2.beginTransaction();

        Brand mergedBrand = (Brand) session2.merge(brand);
        session2.remove(mergedBrand); // Wraz z marką usunęły się wszystkie modele przypisane do marki

        transaction2.commit();
        session2.close();

        HibernateUtils.close();
    }
}
