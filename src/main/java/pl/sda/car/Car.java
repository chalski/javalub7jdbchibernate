package pl.sda.car;

import lombok.Data;

@Data
public class Car {

    private Integer id;
    private Integer brandId;
    private Integer modelId;
    private Integer engineId;

}
