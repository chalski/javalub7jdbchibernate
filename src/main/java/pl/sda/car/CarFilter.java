package pl.sda.car;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.sda.engine.FuelType;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
public class CarFilter {

    private String brandName;
    private String modelName;
    private Integer powerFrom;
    private Integer powerTo;
    private Set<FuelType> fuelTypes = new HashSet<>();
    private Double capacityFrom;
    private Double capacityTo;

}
