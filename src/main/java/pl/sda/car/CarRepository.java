package pl.sda.car;

import lombok.AllArgsConstructor;
import pl.sda.engine.FuelType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class CarRepository {

    private Connection connection;

    public List<Car> findByFilter(CarFilter filter) {
        List<Car> result = new ArrayList<>();
        try {
            StringBuffer sb = new StringBuffer();
            sb.append("SELECT c.id, c.engine_id, c.brand_id, c.model_id, c.mileage FROM car c")
              .append(" JOIN engine e ON e.id = c.engine_id")
              .append(" JOIN brand b ON b.id = c.brand_id")
              .append(" JOIN model m ON m.id = c.model_id")
              .append(" WHERE 1=1");

            List<String> params = new ArrayList<>();
            if(filter.getBrandName()!=null){
                sb.append(" AND b.name = ?");
                params.add(filter.getBrandName());
            }
            if(filter.getModelName()!=null){
                sb.append(" AND m.name = ?");
                params.add(filter.getModelName());
            }
            if(filter.getPowerFrom()!=null){
                sb.append(" AND e.power >= ?");
                params.add(filter.getPowerFrom().toString());
            }
            if(filter.getPowerTo()!=null){
                sb.append(" AND e.power <= ?");
                params.add(filter.getPowerTo().toString());
            }
            if(filter.getCapacityFrom()!=null){
                sb.append(" AND e.capacity >= ?");
                params.add(filter.getCapacityFrom().toString());
            }
            if(filter.getCapacityTo()!=null){
                sb.append(" AND e.capacity <= ?");
                params.add(filter.getCapacityTo().toString());
            }
            if(!filter.getFuelTypes().isEmpty()){
                String fuelTypesSql = filter.getFuelTypes().stream()
                        .map(type -> "?")
                        .collect(Collectors.joining(","));
                sb.append(" AND e.type in ("+ fuelTypesSql +")");
                List<String> fuelTypeNames = filter.getFuelTypes().stream()
                        .map(FuelType::name)
                        .collect(Collectors.toList());
                params.addAll(fuelTypeNames);
            }
            PreparedStatement statement = connection.prepareStatement(sb.toString());

            String sql = sb.toString();
            System.out.println(sql);

            for (int i=1; i<= params.size(); i++) {
                statement.setString(i, params.get(i-1));
            }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Car car = new Car();
                car.setId(resultSet.getInt(1));
                car.setEngineId(resultSet.getInt(2));
                car.setBrandId(resultSet.getInt(3));
                car.setModelId(resultSet.getInt(4));
                result.add(car);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
