package pl.sda.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import pl.sda.Engine2;
import pl.sda.Model2;
import pl.sda.brand.Brand;
import pl.sda.engine.Engine;
import pl.sda.model.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class HibernateUtils {

    private static SessionFactory sessionFactory;

    public static Session getSession() {
        if (sessionFactory == null) {
            buildSessionFactory();
        }
        return sessionFactory.getCurrentSession();
    }

    public static void close() {
        if (sessionFactory != null)
            sessionFactory.close();
    }

    private static void buildSessionFactory() {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

        Map<String, String> properties = new HashMap<>();
        properties.put(Environment.URL, "jdbc:mysql://localhost:3306/javalub7");
        properties.put(Environment.USER, "sda");
        properties.put(Environment.PASS, "pass");
        properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        properties.put(Environment.POOL_SIZE, "2");
        properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        properties.put(Environment.SHOW_SQL, "true");
        properties.put(Environment.DIALECT, "org.hibernate.dialect.MariaDB53Dialect");
        properties.put(Environment.HBM2DDL_AUTO, "update");

        registryBuilder.applySettings(properties);

        StandardServiceRegistry serviceRegistry = registryBuilder.build();

        Metadata metadata = new MetadataSources(serviceRegistry)
                .addAnnotatedClass(Brand.class)
                .addAnnotatedClass(Engine.class)
                .addAnnotatedClass(Model.class)
                .addAnnotatedClass(Engine2.class)
                .addAnnotatedClass(Model2.class)
                .buildMetadata();

        sessionFactory = metadata.buildSessionFactory();
    }

    public static <T> T doInTransaction(Function<Session, T> queryAction) {
        return doInTransaction(getSession(), queryAction);
    }

    public static <T> T doInTransaction(Session session, Function<Session, T> queryAction) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            return queryAction.apply(session);
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            return null;
        } finally {
            if (transaction != null && transaction.getStatus() != TransactionStatus.MARKED_ROLLBACK) {
                transaction.commit();
            }
        }
    }


}
