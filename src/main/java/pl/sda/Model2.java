package pl.sda;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Model2 {

    @Id
    private Integer id;

    private String myName;

    @ManyToMany
    private List<Engine2> engines;
}
