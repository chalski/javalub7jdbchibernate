package pl.sda;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Engine2 {

    @Id
    private Integer id;

    private String myName;

    @Transient // Ta zmienna nie zostanie zapisana w bazie
    private Integer count;

    @ManyToMany
    private List<Model2> model_id;

}
